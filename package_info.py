import re
from collections import KeysView


class PackageInfo:
    """
    Simple utility to collect dpkg package information from /var/lib/dpkg/status
    """

    def __init__(self):
        """
        Initializes PackageInfo and loads package information from the status file
        """

        self._package_dict = {}

        self.reload()

    def reload(self):
        """
        Reloads package information from /var/lib/dpkg/status
        :return:
        """

        current_package = ""  # the name of currently reading package
        current_prop = ""  # the name of currently reading package's property
        with open("/var/lib/dpkg/status") as f:

            # Read dpkg status file line by line
            line = f.readline()

            while line:
                # Detect the package change:
                if line.startswith("Package:"):
                    # Parse package name (skip 'Package: ' at the beginning and the newline at the end)
                    current_package = line[9:-1]

                    # Add new packahe to self._package_dict
                    self._package_dict[current_package] = {}

                # If the line contains information, save it to self._package_dict
                elif len(line) > 1:
                    if line.startswith(" "):
                        # When line starts with " ", it is a part of the same property with the previous line
                        # Append value to self._package_dict
                        self._package_dict[current_package][current_prop] += "\n" + line.strip()
                    else:
                        split_line = line.split(":")

                        # Handle edge case when dpkg status file is not properly formatted
                        if len(split_line) < 2:
                            raise SyntaxError(
                                "Syntax error in /var/lib/dpkg/status: \
                                Row format must be either 'Key: value' or ' continuing previous line'."
                            )

                        # Update current_prop
                        current_prop = split_line[0]

                        # Save value to self._package_dict
                        self._package_dict[current_package][current_prop] = ":".join(split_line[1:])[1:-1]

                line = f.readline()  # Read the next line

        # Reformat needed sections:
        for package in self._package_dict.keys():
            # If package contains Description, remove newline chars at the end of description
            if 'Description' in self._package_dict[package]:
                description = self._package_dict[package]['Description']
                if description[-1] == "\n":
                    self._package_dict[package]['Description'] = description[:-1]

            # If the package contains Depends section, parse it
            if 'Depends' in self._package_dict[package]:
                # Split dependencies string to a list
                depends_list = self._package_dict[package]['Depends'].split(", ")

                processed_depends = []  # Parsed dependencies will be append here

                for dependency in depends_list:  # Loop through each dependency
                    # Re-format dependency to list split by alternate dependencies
                    alternates = dependency.split(" | ")

                    # alternates:
                    #   - ['package (>=x.y.z)'] for normal dependency
                    #   - ['package1 (>=x.y.z)', 'package2 (>=x.y.z)'] for alternative dependencies

                    # Cut out the version numbers (everything after the first space and save them to processed_depends
                    processed_depends.append([alternate.split(" ")[0] for alternate in alternates])

                # Replace newly-formatted Depends section
                self._package_dict[package]['Depends'] = processed_depends

    def get_installed_packages(self) -> KeysView:
        """
        Get all installed packages

        :return: KeysView: (behaves like a collection of package names (str))
        """

        return self._package_dict.keys()

    def get_package_info(self, package_name: str) -> dict:
        """
        Search package information by package name

        returns info in dict:
        {
            "name": "package",
            "description": "A sample package"
            "dependencies": ["package1", "package2"]
            "reverse_dependencies": ["package5", "package6", "package7"]
        }

        :param package_name: the name of the package (str containing ([a-z][0-9]+-.))
        :return: information found by package name (dict; empty if no packages found with given name)
        """

        assert re.match(r'[a-z0-9.+-]+$', package_name), "Package names can contain only ([a-z][0-9]+-.)"

        package_info = {}

        package = self._package_dict.get(package_name, None)

        # parse package info if package found
        if package:
            # Set package name
            package_info["name"] = package_name

            # Set package description
            package_info["description"] = package.get("Description", [])

            # Set package dependencies
            package_info["dependencies"] = []
            dependencies = package.get("Depends", [])
            for dependency in dependencies:
                if len(dependency) == 1:
                    # When dependency does not have alternatives
                    package_info["dependencies"].append(dependency[0])

                else:
                    # When dependency has alternatives,
                    # find the first package name in alternates which exits in self._packages_dict
                    for alternate in dependency:
                        if alternate in self._package_dict.keys():
                            package_info["dependencies"].append(alternate)
                            break

            # Set package reverse dependencies
            package_info["reverse_dependencies"] = []
            for name, details in self._package_dict.items():
                # Test package by package if the package_name exists in Depends section
                for dependency in details.get('Depends', []):
                    if package_name in dependency:
                        # When reverse dependency found, append it to package_info
                        package_info["reverse_dependencies"].append(name)

        return package_info


if __name__ == '__main__':
    """
    An example of how to use PackageInfo
    """

    packages = PackageInfo()

    all_packages = packages.get_installed_packages()

    print(str(len(all_packages)) + " packages installed on this system.")

    info = packages.get_package_info("libjpeg8")

    # Print package information in:
    for key, value in info.items():
        if type(value) == list:
            print(key + ":\n  - " + "\n  - ".join(value))
        else:
            print(key + ":\n  " + "\n  ".join(str(value).split('\n')))
