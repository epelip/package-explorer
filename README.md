# package-explorer
**package-explorer** is a simple HTTP interface to explore package information
about packages installed on a Debian based os. The package information is read from ```/var/lib/dpkg/status```.

At the index page, there is a list of all installed packages. Each package name is a link to the page that contains more
information about the package. The package information contains the package name, package description, dependencies and
reverse-dependencies (installed packages which depends on the package).

## Demo
The staging version of package-explorer is hosted at [Heroku](https://pkgexplore.herokuapp.com/)

## Install dependencies
This project is written in python3 and python packages.
All required packages are listed in requirements.txt with tested versions.

Inside project root with ```python3``` and ```pip3``` installed:
```shell script
pip3 install -r requirements.txt
```

## Run in development mode
To run package-explorer in development mode, run the following command inside the project folder:
```shell script
python3 main.py
```

The development server is now running at [localhost:5000](http://localhost:5000).

## Deploying
> It is recommended to setup a [venv](https://docs.python.org/3/tutorial/venv.html) when running the app in production mode

To run package-explorer in production mode on port 8000:
```shell script
PORT=8000 uwsgi package-explorer.ini
```
The production server is now running on the specified port.
