
def order(names) -> dict:
    """
    A helper function to sort package names to a dict alphabetically by the first letter

    example:
    - input ["bzip2", "apt-utils", "bash", "adduser", "build-essential", "apt"]:
    - return:
    {
        "A": ["adduser", "apt", "apt-utils"],
        "B": ["bash", "build-essential", "bzip2"]
    }

    :param names: iterable object containing strings
    :return: dict
    """

    dictionary = dict()

    for name in sorted(names):
        label = name[0].upper()

        if label in dictionary.keys():
            dictionary[label].append(name)
        else:
            dictionary[label] = [name]

    return dictionary
