from package_info import PackageInfo
from utils import order

from flask import Flask, render_template, abort

app = Flask(__name__)

packages = PackageInfo()


@app.route('/')
def index():
    """Renders a list of installed packages"""
    packages.reload()
    return render_template("package_list.html", packages=order(packages.get_installed_packages()))


@app.route('/packages/<package_name>')
def get_package(package_name):
    """Renders information about requested package"""
    packages.reload()
    package = packages.get_package_info(package_name)

    # Redirect to 404 if package not found
    if not package:
        abort(404)

    return render_template("package_info.html", package=package)


if __name__ == '__main__':
    app.run(debug=True)
